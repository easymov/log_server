^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package log_server
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.1.1 (2017-03-27)
------------------
* First public release
* Contributors: Gérald Lelong
